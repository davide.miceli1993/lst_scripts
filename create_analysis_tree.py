"""Create the directory structure for an analysis.
    Change the default value for the Parent folder in the argparse
    
        Parent_Folder
        └──DL1
            ├── source
            │   └── night
            │      └── version
            │           └── cleaning
            DL2
            ├── source
            │   └── night
            │      └── version
            │           └── cleaning
            DL3
            └── source
                └── night
                   └── version
                        └── cleaning
"""

import os
import argparse
from argparse import RawTextHelpFormatter


def makedir(name):
    """
    Create folder if non-existent and output OS error if any.

    Parameters
    ----------
    name : str
        Name of the analysis.
    """

    # Create target Directory if don't exist
    if not os.path.exists(name):
        os.makedirs(name)
        print("Directory ", name, " created ")
    else:
        print("Directory ", name, " already exists")
    return None


def main():
    # define command-line arguments
    description = 'Create a directory structure'

    parser = argparse.ArgumentParser(
        description=description,
        formatter_class=RawTextHelpFormatter
    )

    parser.add_argument(
        '--main_dir', type=str, required=False, dest='main_dir',
        default='/fefs/aswg/workspace/alice.donini/Analysis/data',
        help='Path to parent folder'
    )

    parser.add_argument(
        '--source', type=str, required=True, dest='source',
        help='Source name'
    )

    parser.add_argument(
        '--night', nargs='+', type=int, required=True, dest='night',
        help='Night date'
    )

    parser.add_argument(
        '--version', type=str, required=False, dest='version',
        default='v0.9', help='lstchain version (default: %(default)s)'
    )

    parser.add_argument(
        '--cleaning', type=str, required=False, dest='cleaning',
        default='tailcut84', help='Cleaning type (default: %(default)s)'
    )

    parser.add_argument(
        '--suffix', type=str, dest='suffix',default=None,
                            help='Tag to be added to the analysis.'
    )

    args = parser.parse_args()
    
    # Define analysis directories and subdirectories
    data_dir = ['DL1', 'DL2', 'DL3']
    nights = args.night

    for night in nights:
        structure = f'{args.source}/{args.suffix + '/' if args.suffix is not None else ''}{night}/{args.version}/{args.cleaning}'
        for folder in data_dir:
            dirName = os.path.join(f'{args.main_dir}', str(folder), structure)
            makedir(dirName)

    print(f'Directory structure for analysis on {args.source} was created.')


if __name__ == "__main__":
    main()
