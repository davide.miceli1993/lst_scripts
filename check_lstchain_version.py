import tables


run = '8623'
data_dir = '/fefs/aswg/data/real/DL1'     # path to DLx directory
night = '20220603'          # day of observation
version = 'v0.9'                      # v0.7.3, v0.8.4, v0.9, v0.9.1
cleaning = 'tailcut84'

path = data_dir + '/' + night + '/' + version + '/' + cleaning
filename = 'dl1_LST-1.Run0' + run + '.h5'
file = path + '/' + filename
print("file: ", file)

with tables.open_file(file) as file:
    print(file.root._v_attrs["LSTCHAIN_VERSION"])
