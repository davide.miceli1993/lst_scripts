import os
from pathlib import Path
from utils import (
    parse_arguments,
    get_config,
    manage_submission,
    get_coordinates,
)

# Argument parser
args = parse_arguments(description="DL2 to DL3 converter",
                       add_job=True,
                       add_dl3=True,
                       add_common=True)

config_file = get_config(args.config)

# manage the target
ra, dec = get_coordinates(args)

# manage the cuts
if args.cut_file is not None:
    cut_file = config_file['cut_folder'] + "/" + args.cut_file
    print("Using cut file:", cut_file)
else:
    print("Standard cuts will be used in the analysis.")

irf = config_file['irf_file']

for night in config_file['dl2_data']['night']:
    # output directory
    dl3_out_dir = config_file['base_dir'] + '/DL3/' + args.source_name + '/' + ('/' + config_file['suffix'] if 'suffix' in config_file else '') + str(night) + '/' + config_file['dl2_data']['version'] + '/' + config_file['dl2_data']['cleaning']
    if args.outdir is not None:
        dl3_out_dir = args.outdir

    dl2_dir = config_file['base_dir'] + '/DL2/' + args.source_name + ('/' + config_file['suffix'] if 'suffix' in config_file else '') + '/' + str(night) + '/' + config_file['dl2_data']['version'] + '/' + config_file['dl2_data']['cleaning']
    files = Path(dl2_dir).glob('dl2*.h5')
    
    for file in files:
        dl3_cmd = (
            f'lstchain_create_dl3_file -d {file} '
            f'-o {dl3_out_dir} '
            f'--input-irf {irf} '
            f'--source-name {args.source_name} '
            f'--source-ra "{ra}deg" '
            f'--source-dec "{dec}deg" '
            f'{"--overwrite " if args.globber is not False else ""}'
        )
        if args.cut_file is not None:
            dl3_cmd += f'--config {args.cut_file}'
        if args.gh_cut:
            dl3_cmd += f'--fixed-gh-cut {args.gh_cut}'
        if args.theta_cut:
            dl3_cmd += f'--fixed-theta-cut {args.theta_cut}'

        if args.verbose:
            print('\n')
            print(dl3_cmd)
        if not(args.dry):
            if args.submit:
                # create the script that will be submited and return the name of the script in the variable scriptname
                scriptname = manage_submission(args, config_file, dl3_cmd, os.path.basename(file))
                os.system("sbatch " + scriptname)
            else:
                print('Interactive conversion of the DL2 to DL3.')
                os.system(dl3_cmd)
