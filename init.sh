# To activate the pre-installed conda env and the latest lstchain env, uncomment the following two lines
#source /fefs/aswg/software/conda/etc/profile.d/conda.sh
#conda activate lstchain-v0.9.1

# personal lstchain conda env comment if you want to use the pre-installed one
conda activate lst-dev 

export CODE_DIR=/../lst_scripts
export PYTHONPATH=$CODE_DIR:$PYTHONPATH
export CONFIG_FOLDER=../
