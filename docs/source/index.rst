
A "How to" Guide for LST analysis at IT
===============================================================================================

This document aims to describe how to perform an analysis at the IT cluster in La Palma using the scripts hosted on the `GitLab repository <https://gitlab.com/davide.miceli1993/lst_scripts>`_ .


.. toctree::
   :maxdepth: 1
   :caption: Contents

   install/index
   usage/index


.. Indices and tables
.. ==================

.. * :ref:`genindex`
.. * :ref:`modindex`
.. * :ref:`search`