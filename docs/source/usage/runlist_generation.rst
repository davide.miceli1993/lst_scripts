.. _runlist_generation:

Runlist generation
==================

The script `create_run_list.py` reads in the DL1 data check information from night-wise hdf5 files, uses them to select runs for analysis and provides a file with a list of the selected runs and their corrisponding night.

If no night is specified trough the option ``--night``, then the script reads in all the DL1 data check files available and this could require some time.
The default folder in which the DL1 data check files are searched is `/fefs/aswg/data/real/OSA/DL1DataCheck_LongTerm/v0.9/20*/`.
The produced runlist file is saved in the ``$CONFIG_FOLDER`` folder specified in the initial settings and can be then used as input to the analysis scripts.

For the selection of the runs:
* Selects runs in which the telescope was pointing towards the source of interest specified.
* Excludes runs in which interleaved pedestals or flatfield events were missing.


.. code-block::
    
    usage: create_run_list.py [-h] [--verbose] --source_name SOURCE_NAME [--ra RA] [--dec DEC] [--night NIGHT [NIGHT ...]]

    Runlist creation

    optional arguments:
      --dec DEC             Dec coordinate of the target. To add if you want to use custom position
      --night NIGHT [NIGHT ...]
                            Night of the observation in the format YYYYMMDD. More nights can be specified
      --ra RA               RA coordinate of the target. To add if you want to use custom position
      --source_name SOURCE_NAME, -n SOURCE_NAME
                            Name of the source
      --verbose, -v         Increase output verbosity
      -h, --help            show this help message and exit

In the script it is assumed that the source coordinates are the same as the telescope pointing. So the source name argument ``-n`` is mandatory and is used to find through Astropy the source coordinates, needed from the script to select the runs.

If source coordinates and telescope pointing differ, then specify custom coordinates with arguments ``--ra`` and ``--dec``. These values will overwrite the values obtained through Astropy.

Examples of how to run the script:

.. code-block::
    
    python create_run_list.py -n GRB220527A -v
    python create_run_list.py -n GRB220527A --ra 323.56990 --dec -14.90 -v
    python create_run_list.py -n GRB210704A --ra 163.73 --dec 58.86 --night 20210705 20210704 20210706 -v
