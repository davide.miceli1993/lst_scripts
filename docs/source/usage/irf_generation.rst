.. _irf_generation:

IRF generation
==============

The script `create_irf.py` allows to create IRFs FITS file from given MC DL2 files and selection cuts. It makes use of the configuration file `config_IRF.yaml`:

.. code-block:: yaml

  # path to folder where output data are saved
  output_data_folder: ./

  # values for automatic mc data selection
  DL2_dir: /fefs/aswg/data/mc/DL2/20200629_prod5_trans_80  # path to DL2 directory
  version: 20210416_v0.7.3_prod5_trans_80_local_taicut_8_4 # production version, other possibility:
                                                           # 20210923_v0.7.5_prod5_trans_80_dynamic_cleaning
  zenith: 20                                               # 20 or 40
  direction: south                                         # pointing direction, e.g. south, north
  gammatype: point                                         # type of gammas, e.g. point, diffuse

Edit the configuration file: change the path of the output folder based on your working directory and modify the information used to select automatically the MC files.

The script:

.. code-block:: bash

    usage: create_irf_files.py [-h] [--prod PROD] [--outdir OUTDIR] [--config CONFIG]
                           [--config-analysis CONFIG_ANALYSIS] [--verbose] [--submit] [--dry] [--globber]
                           [--gh_cut GH_CUT] [--theta_cut THETA_CUT] [--obs_time OBS_TIME]

    Create IRF from MC files

    optional arguments:
      --config CONFIG, -c CONFIG
                            Specify a personal config file for the analysis
      --config-analysis CONFIG_ANALYSIS
                            Specify a config file which described analysis profile to use
      --dry                 Make a dry run, no true submission
      --gh_cut GH_CUT       Fixed selection cut for gh_score (gammaness)
      --globber, -g         If True, overwrites existing output file without asking
      --obs_time OBS_TIME   Observation time for IRF in hours
      --outdir OUTDIR, -o OUTDIR
                            Directory to store the output
      --prod PROD, -p PROD  Prod to use (default: v0.8.4)
      --submit              Submit the cmd to slurm on site
      --theta_cut THETA_CUT
                            Fixed selection cut for theta
      --verbose, -v         Increase output verbosity
      -h, --help            show this help message and exit

If no analysis configuration file is given, default selection parameters are used (gh=0.6, theta=0.2, time=50h).
For a custom production copy and append the relevant parameters into a custom configuration file, that can be passed with the option ``--config-analysis``. You can take as example the file `irf_tool_config.json`, that can be found in lstchain in `docs/examples/`.
There is the possibility to pass the selection cuts and overwrite the default values directly from the command-line: ``--gh_cut``, ``--theta_cut`` and ``--obs_time``.
These optional arguments can not be used if a custom configuration file is provided.

There is no implementation of job submission (option ``--submit`` is not available).

Examples:

.. code-block:: bash

    python create_irf.py -c config_IRF.yaml --config-analysis ../config/irf_tool_config.json -v -g

    python create_irf.py -c config_IRF.yaml --gh_cut 0.2 -v -g

