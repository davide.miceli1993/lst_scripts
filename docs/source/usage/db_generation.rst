.. _db_generation:

Database generation
===================

The script `GenerateDB_Seiya.py` generates a database file of the data taken by LST from the excel file Seiya is keepeing up to date.
The databse is used to extract info about target and runs, so that the wanted data files can be retrieved at the IT.

To generate the database file run:

.. code-block::

    python GenerateDB.py

Add argument ``-n`` if you want to change the name given to the file. Default name is `database.csv`.

A run selection can be made later. Several ways of getting a run list are implemented in each script:

* The user provides the run number and night with a file;
* Selections of the runs based on the `night` provided by the user in the configuration file.
* Selections of the runs based on the `TCU name` provided by the user
* Selections of the runs based on the `angular distance`, i.e the maximum distance in degrees between the source position and the run pointing position

.. note::

    Remember to generate a new database file every now and then to be up to date with the latest data taken.


.. warning::

    The search of runs through the database has an issue on the dates at the moment. The database is generated from the drive log, so all the runs taken after the midnight are saved under the following day. This doesn't happen at the IT, where the runs are stored under the day of the data taking night.
