.. _index_generation:

Index creation
==============

Gammapy needs an index file to read the generated DL3 fits file (:ref:`dl3`). This can be created using the `create_DL3_index.py` script.
The script makes use of the same configuration file of the script to generate DL3 files: `config_dl2_to_dl3.yaml`.

Usage:

.. code-block:: bash

    usage: create_DL3_index.py [-h] [--verbose] --source_name SOURCE_NAME --config CONFIG [--dry]

    DL3 index maker

    optional arguments:
      --config CONFIG, -c CONFIG
                            Specify a personal config file for the analysis
      --dry                 Make a dry run, no true submission
      --source_name SOURCE_NAME, -n SOURCE_NAME
                            Name of the source
      --verbose, -v         Increase output verbosity
      -h, --help            show this help message and exit


The command is pretty similar to the one for the DL3 generation. The code is fast, so no submission process has been implemented.

.. code-block:: bash

    python create_DL3_index.py -c config_dl2_to_dl3_Crab2020.yaml --source_name Crab -v

After running the script, you should have two files in the DL3 data directory: `hdu-index.fits.gz` and `obs-index.fits.gz`.

.. note::

    The index files have to be in the same folder of the related DL3 file.
