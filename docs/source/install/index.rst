.. _install:

Installation
============

First you must make sure you have ``lstchain`` installed, since it is needed to run the scripts. If you don't have ``lstchain``, you should get the latest stable relase or the development version if you also want to participate in its development. For the installation of either version, please refer to `lstchain <https://github.com/cta-observatory/cta-lstchain>`_.

.. note::
  Under the ASWG workspace at the IT you can find both the Anaconda package and the latest stable relase of ``lstchain`` installed.
  You can use these pre-installed versions, or create your own installation/virtual environment under your personal workspace.

Create a `init.sh` file with variables useful for later:

.. code-block:: bash

    conda activate lst-dev
    export CODE_DIR = ../lst_scripts
    export PYTHONPATH = $CODE_DIR:$PYTHONPATH
    export CONFIG_FOLDER = ../

``CODE_DIR`` is the directory in which you will have the scripts, while ``CONFIG_FOLDER`` is the directory where the configuration files should be stored.
Fill the missing path `../` with the specific path of your installation.
The first command activate the lstchain conda environment that you installed. If your environment is called differently, change the name.

At the IT it is also installed a common lstchain conda environment. This conda environment is kept up to date with every new release, only change the name of the environment accordingly to the version you need of ``lstchain``.
The available environment can be seen at ``/fefs/aswg/software/conda/envs/``.

The `init.sh` file will be:

.. code-block:: bash
    
    source /fefs/aswg/software/conda/etc/profile.d/conda.sh
    conda activate lstchain-v0-9.1
    export CODE_DIR = ../lst_scripts
    export PYTHONPATH = $CODE_DIR:$PYTHONPATH
    export CONFIG_FOLDER = ../

An `init.sh` file example is already present in the repository. You can edit it with your directory paths and before starting the analysis run:

.. code-block::
    
    source init.sh

This way, the lstchain environment and all the variable needed for the analysis will be set.
