import numpy as np
import pandas as pd
from lstchain.io.io import dl2_params_src_dep_lstcam_key, dl2_params_lstcam_key
from lstchain.reco.utils import get_effective_time
from ctapipe.containers import EventType
import matplotlib.pyplot as plt
import astropy.units as u
from os import walk
from gammapy.stats import WStatCountsStatistic

dates=['20210809', '20210810', '20210812']

gammaness_cuts=[0.7, 0.75, 0.8, 0.85,0.87, 0.9, 0.95]

data_dl3=pd.DataFrame()
data_dl3_src_dep=pd.DataFrame()
obstime_real=0 * u.s

for date in dates:
    sourcedir="/fefs/aswg/workspace/maria.bernardos/LSTanalysis/full_analysis/data/real/DL2/"+date+"/v0.7.3/tailcut84/merged/"
    filenames = next(walk(sourcedir), (None, None, []))[2]
    sourcefile=sourcedir+filenames[0]


    for sourcefile in filenames:
        print("File: "+sourcefile)
        sourcefile=sourcedir+sourcefile
        data_dl3_run=pd.read_hdf(sourcefile, key=dl2_params_lstcam_key)
        data_dl3_src_dep_run=pd.read_hdf(sourcefile, key=dl2_params_src_dep_lstcam_key)
        obstime_real_run = get_effective_time(data_dl3_run)[0]

        data_dl3=pd.concat([data_dl3, data_dl3_run])
        data_dl3_src_dep=pd.concat([data_dl3_src_dep, data_dl3_src_dep_run])
        obstime_real+=obstime_real_run

emin_sensitivity = 0.01 * u.TeV  # mc_par_g['emin']
emax_sensitivity =  10 * u.TeV  # mc_par_g['emax']
n_bins_energy=15
log_energy = np.linspace(np.log10(emin_sensitivity.to_value()),
                     np.log10(emax_sensitivity.to_value()),
                     n_bins_energy + 1)

alpha_on = np.array(data_dl3_src_dep['(\'on\', \'alpha\')'])
log_reco_energy_on = np.array(data_dl3_src_dep['(\'on\', \'log_reco_energy\')'])
gammaness_on = np.array(data_dl3_src_dep['(\'on\', \'gammaness\')'])

alpha_off_090 = np.array(data_dl3_src_dep['(\'off_090\', \'alpha\')'])
log_reco_energy_off_090 = np.array(data_dl3_src_dep['(\'off_090\', \'log_reco_energy\')'])
gammaness_off_090 = np.array(data_dl3_src_dep['(\'off_090\', \'gammaness\')'])

alpha_off_180 = np.array(data_dl3_src_dep['(\'off_180\', \'alpha\')'])
log_reco_energy_off_180 = np.array(data_dl3_src_dep['(\'off_180\', \'log_reco_energy\')'])
gammaness_off_180 = np.array(data_dl3_src_dep['(\'off_180\', \'gammaness\')'])

alpha_off_270 = np.array(data_dl3_src_dep['(\'off_270\', \'alpha\')'])
log_reco_energy_off_270 = np.array(data_dl3_src_dep['(\'off_270\', \'log_reco_energy\')'])
gammaness_off_270 = np.array(data_dl3_src_dep['(\'off_270\', \'gammaness\')'])

leakage_intensity_width_2 = np.array(data_dl3.leakage_intensity_width_2)
intensity = np.array(data_dl3.intensity)
alt = np.array(data_dl3.alt_tel)
event_id = np.array(data_dl3.event_id)
wl = np.array(data_dl3.wl)
ucts_event_type = np.array(data_dl3.ucts_trigger_type)
event_type = np.array(data_dl3.event_type)

intensity_cut_high = 1e10
#alt_min = 55 * np.pi / 180 # rad
alt_min=0
wl_cut = 0.0
intensity_cut=50

for gammaness_cut in gammaness_cuts:

    condition_alpha_off_090 = (log_reco_energy_off_090 < np.log10(emax_sensitivity.to_value())) \
                              & (log_reco_energy_off_090 >= np.log10(emin_sensitivity.to_value())) \
                              & (gammaness_off_090 > gammaness_cut) \
                              & (intensity > intensity_cut) \
                              & (intensity < intensity_cut_high) \
                              & (alt > alt_min) \
                              & (wl > wl_cut) \
                              & (event_type != EventType.FLATFIELD.value)\
                              & (event_type != EventType.SKY_PEDESTAL.value)\
                              & (leakage_intensity_width_2 < 0.2)

    plt.hist(alpha_off_090[(condition_alpha_off_090)],
             density=False, bins=30, range=(0,90), histtype='step', label='OFF 90')

    condition_alpha_off_180 = (log_reco_energy_off_180 < np.log10(emax_sensitivity.to_value())) \
                            & (log_reco_energy_off_180 >= np.log10(emin_sensitivity.to_value())) \
                            & (gammaness_off_180 > gammaness_cut) \
                            & (intensity > intensity_cut) \
                            & (intensity < intensity_cut_high) \
                            & (alt > alt_min) \
                            & (wl > wl_cut) \
                            & (event_type != EventType.FLATFIELD.value)\
                            & (event_type != EventType.SKY_PEDESTAL.value)\
                            & (leakage_intensity_width_2 < 0.2)

    plt.hist(alpha_off_180[(condition_alpha_off_180)],
             density=False, bins=30, range=(0,90), histtype='step', label='OFF 180')


    condition_alpha_off_270 = (log_reco_energy_off_270 < np.log10(emax_sensitivity.to_value())) \
                              & (log_reco_energy_off_270 >= np.log10(emin_sensitivity.to_value())) \
                              & (gammaness_off_270 > gammaness_cut) \
                              & (intensity > intensity_cut) \
                              & (intensity < intensity_cut_high) \
                              & (alt > alt_min) \
                              & (wl > wl_cut) \
                              & (event_type != EventType.FLATFIELD.value)\
                              & (event_type != EventType.SKY_PEDESTAL.value)\
                              & (leakage_intensity_width_2 < 0.2)

    plt.hist(alpha_off_270[(condition_alpha_off_270)],
             density=False, bins=30, range=(0,90), histtype='step', label='OFF 270')

    condition_alpha_on = (log_reco_energy_on < np.log10(emax_sensitivity.to_value())) \
                         & (log_reco_energy_on >= np.log10(emin_sensitivity.to_value())) \
                         & (gammaness_on > gammaness_cut) \
                         & (intensity > intensity_cut) \
                         & (intensity < intensity_cut_high) \
                         & (alt > alt_min) \
                        & (wl > wl_cut) \
                        & (event_type != EventType.FLATFIELD.value)\
                        & (event_type != EventType.SKY_PEDESTAL.value)\
                        & (leakage_intensity_width_2 < 0.2)

    plt.hist(alpha_on[(condition_alpha_on)],
             density=False, bins=30, range=(0,90), histtype='step', label='ON')

    data_on=alpha_on[(condition_alpha_on)]
    data_off_090=alpha_off_090[(condition_alpha_off_090)]
    data_off_180=alpha_off_090[(condition_alpha_off_180)]
    data_off_270=alpha_off_090[(condition_alpha_off_270)]

    ALPHA_CUT=10

    N_on=len(data_on[(data_on<ALPHA_CUT)])
    N_off_090=len(data_off_090[(data_off_090<ALPHA_CUT)])
    N_off_180=len(data_off_180[(data_off_180<ALPHA_CUT)])
    N_off_270=len(data_off_270[(data_off_270<ALPHA_CUT)])
    N_on_norm=len(data_on[(data_on>20) & (data_on < 90)])
    Noff_090_norm=len(data_off_090[(data_off_090>20) & (data_off_090 < 90)])
    Noff_180_norm=len(data_off_180[(data_off_180>20) & (data_off_180 < 90)])
    Noff_270_norm=len(data_off_270[(data_off_270>20) & (data_off_270 < 90)])

    N_off=(N_off_090+N_off_180+N_off_270)/3
    N_off_norm=(Noff_090_norm+Noff_180_norm+Noff_270_norm)/3
    alpha=N_off_norm/N_on_norm

    stat = WStatCountsStatistic(n_on=N_on, n_off=N_off_090, alpha=alpha)

    significance_lima = stat.sqrt_ts

    print("\n")
    print("GAMMANESS CUT: %.2f" % gammaness_cut)
    print("SIGNIFICANCE: %.3f" % significance_lima)
    print("N ON: %d" % N_on)
    print("N OFF: %d"%N_off)
    print("S/N RATIO: %.4f " % ((N_on - N_off)/N_off))
    print("OBSERVING TIME: %.2f hours" % obstime_real.to_value(u.h))
    plt.legend()
    plt.show()
