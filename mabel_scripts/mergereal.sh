#!/bin/bash

#DATES=( 20210810 20210812 20210813 20210814 20210815 20210830 20210829 20210828 20210827 20210902 20210901 )
DATES=( 20201118 )
VERS="v0.7.3"
#SUFFIX="_tuned_main_island"
TAILCUT="tailcut84"
BASEDIR=/fefs/aswg/workspace/maria.bernardos/LSTanalysis/full_analysis/data/real/DL2/
#OUTBASEDIR=/fefs/aswg/workspace/maria.bernardos/LSTanalysis/full_analysis/data/real/DL1/
SCRIPT="/fefs/aswg/workspace/maria.bernardos/GitHub/cta-lstchain/lstchain/scripts/lstchain_merge_hdf5_files.py"

#SUFFIXES=( '_tuned' '_tuned_main_island' '_tuned_reduced' '_tuned_main_island_reduced' )
SUFFIXES=( 'tuned_dynamic_cleaning_source_independent' )

for SUFFIX in "${SUFFIXES[@]}"
do
    for DATE in "${DATES[@]}"
    do
        VERS="v0.7.3"

        DIR=$BASEDIR/$DATE/$VERS$SUFFIX/$TAILCUT/
        mkdir -p ${DIR}/merged
        FILES=`ls $DIR/dl2*.h5`
        current_run=0000
        for f in $FILES
        do
            b=$(basename $f)
            run=${b:14:-8}
            if [ "$run" -eq "$current_run" ]
            then
                continue

            else
                current_run=$run
                echo $run
                OUTNAME=${DIR}/merged/'dl2_LST-1.Run0'$run'.h5'
                if [ ! -f $OUTNAME ]
                then
                    srun -p long -o $DIR/run_${run}_mergereal.log python $SCRIPT -d $DIR -o $OUTNAME -r $run --smart False &
                fi
            fi
        done
        #for run in "${RUNS[@]}"
        #do
        #

        #done
    done
done
