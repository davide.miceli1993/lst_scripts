#!/bin/bash
BASEDIR="/fefs/aswg/workspace/maria.bernardos/LSTanalysis/full_analysis/data/real/DL3/"

DATES=( crabnovember )
TAILCUT=tailcut84

SUFFIX="tuned_dynamic_cleaning_source_independent"
CUT='crabcutloosest'
VERS="v0.7.3"

SCRIPT="/fefs/aswg/workspace/maria.bernardos/GitHub/cta-lstchain/lstchain/tools/lstchain_create_dl3_index_files.py"

for date in "${DATES[@]}"
do
    DIR=$BASEDIR/$date/$VERS$SUFFIX/${TAILCUT}${CUT}/
    #DIR=$BASEDIR/$date/
    #srun -o out.txt python $SCRIPT -d $DIR --overwrite &
    srun python $SCRIPT -d $DIR --overwrite &
done
