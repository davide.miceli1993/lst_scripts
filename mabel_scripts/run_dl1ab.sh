#!/bin/bash

DATES=( 20201118 )
#DATES=( 20210813 )
SUFFIX="tuned_dynamic_cleaning"
PATH_MODELS="/fefs/aswg/workspace/maria.bernardos/LSTanalysis/full_analysis/models/"
SCRIPT=/fefs/aswg/workspace/maria.bernardos/GitHub/cta-lstchain/lstchain/scripts/lstchain_dl1ab.py
BASEDIR="/fefs/aswg/data/real/DL1/"
VERS="v0.7.1"
TAILCUT="tailcut84"
CONFIG=$PATH_MODELS/${SUFFIX}/lstchain_standard_8-4_config.json


for DATE in "${DATES[@]}"
do
    DATE_FORMATTED=$(date -d ${DATE} +%y_%m_%d)
    CALDIR=/fefs/aswg/data/real/calibration/${DATE}/${VERS}/
    PEDESTAL=$(ls ${CALDIR}/drs4_pedestal*)
    CALIB=$(ls ${CALDIR}/calibration*)
    TIME_CALIB=$(ls ${CALDIR}/time_calibration*.hdf5)
    POINTING_FILE=/fefs/aswg/data/real/monitoring/DrivePositioning/drive_log_${DATE_FORMATTED}.txt
    RUN_SUMMARY=/fefs/aswg/data/real/monitoring/RunSummary/RunSummary_${DATE}.ecsv
    INPUT_DIR=${BASEDIR}/${DATE}/${VERS}/${TAILCUT}/
    PATH_OUTPUT=/fefs/aswg/workspace/maria.bernardos/LSTanalysis/full_analysis/data/real/DL1/${DATE}/$VERS$SUFFIX/$TAILCUT/
    mkdir -p ${PATH_OUTPUT}
    mkdir -p jobs/${SUFFIX}/dl1ab_real/${DATE}/
    for file in $(ls ${INPUT_DIR}/ | grep -v datacheck | grep .h5); do
        if [ ! -f $PATH_OUTPUT$file ]
        then
            echo "File $file"
            rm -rf jobs/${SUFFIX}/dl1ab_real/${DATE}/run_${file}.sh

            echo "#!/bin/bash
#SBATCH -N 1
ulimit -l unlimited
ulimit -s unlimited
ulimit -a
       python $SCRIPT -f $INPUT_DIR$file -o $PATH_OUTPUT$file -c $CONFIG --no-image True --pedestal-cleaning True &"  >> jobs/${SUFFIX}/dl1ab_real/${DATE}/run_${file}.sh
        fi
    done
    chmod gu+x jobs/${SUFFIX}/dl1ab_real/${DATE}/run_*
done
