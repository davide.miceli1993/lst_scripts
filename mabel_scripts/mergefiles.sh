#!/bin/bash

#TYPES=( "gamma-diffuse" "gamma" "proton" "electron")
TYPES=( "gamma" )
OFFSETS=( "off0.4deg" )
BASEDIR=/fefs/aswg/workspace/maria.bernardos/LSTanalysis/full_analysis/data/mc/DL1/
#BASEDIR="/fefs/aswg/workspace/maria.bernardos/LSTanalysis/data_MC_comp/data/DL1/mc/"
ZENITHS=( "zenith_40deg" )
POINTINGS=( "north_pointing" )
PROD="20200629_prod5_trans_80"
SCRIPT="/fefs/aswg/workspace/maria.bernardos/GitHub/cta-lstchain/lstchain/scripts/lstchain_merge_hdf5_files.py"
SUFFIX='tuned_dynamic_cleaning'
SET=testing

for particle in "${TYPES[@]}"
do
    for zenith in "${ZENITHS[@]}"
    do
        for pointing in "${POINTINGS[@]}"
        do
            if [ $particle == "gamma" ]
            then
                for offset in "${OFFSETS[@]}"
                do
                    INPATH=${BASEDIR}/${PROD}_${SUFFIX}/${particle}/${zenith}/${pointing}/${offset}/${SET}
                    PATH_OUTPUT=${BASEDIR}/${PROD}_${SUFFIX}/${particle}/${zenith}/${pointing}/${offset}/
                    OUTNAME=$PATH_OUTPUT/${particle}_${PROD}_${SUFFIX}_${SET}.h5

                    srun -o $PATH_OUTPUT/mergelog.log python $SCRIPT -d $INPATH -o $OUTNAME --smart False &

                    #srun -o $PATH_OUTPUT/mergelog.log python $SCRIPT -d $INPATH -o $OUTNAME --smart False &
                done
            else
                INPATH=${BASEDIR}/${PROD}_${SUFFIX}/${particle}/${zenith}/${pointing}/${SET}
                PATH_OUTPUT=${BASEDIR}/${PROD}_${SUFFIX}/${particle}/${zenith}/${pointing}/
                OUTNAME=$PATH_OUTPUT/${particle}_${PROD}_${SUFFIX}_${SET}.h5
                srun -o $PATH_OUTPUT/mergelog.log python $SCRIPT -d $INPATH -o $OUTNAME --smart False &
                #srun -o $PATH_OUTPUT/mergelog.log python $SCRIPT -d $INPATH -o $OUTNAME --smart False &
            fi
        done
    done
done
