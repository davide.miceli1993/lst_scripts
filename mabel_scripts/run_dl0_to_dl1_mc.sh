
#!/bin/bash

#TYPES=( "gamma" "gamma-diffuse" "proton" "electron" )
TYPES=( "electron")
#Create outputs if they dont exist
OFFSETS=( "off0.0deg" "off0.4deg" )
ZENITHS=( "zenith_40deg" )
POINTINGS=( "south_pointing" )
BASEDIR="/fefs/aswg/data/mc/DL0/"
PROD="20200629_prod5_trans_80"
SUFFIX='only_main_island'
PATH_MODELS="/fefs/aswg/workspace/maria.bernardos/LSTanalysis/full_analysis/models/"
CONFIG=$PATH_MODELS/${SUFFIX}/lstchain_standard_config_tailcut84.json
SCRIPT=/fefs/aswg/workspace/maria.bernardos/GitHub/cta-lstchain/lstchain/scripts/lstchain_mc_r0_to_dl1.py

for particle in "${TYPES[@]}"
do
    for zenith in "${ZENITHS[@]}"
    do
        for pointing in "${POINTINGS[@]}"
        do
            if [ $particle == "gamma" ]
            then
                for offset in "${OFFSETS[@]}"
                do
                    INPUT_DIR=${BASEDIR}/${PROD}/${particle}/${zenith}/${pointing}/${offset}/
                    PATH_OUTPUT=/fefs/aswg/workspace/maria.bernardos/LSTanalysis/full_analysis/data/mc/DL1/${PROD}/${particle}/${zenith}/${pointing}/${offset}
                    mkdir -p ${PATH_OUTPUT}
                    for file in $(ls ${INPUT_DIR}/ | grep simtel); do
                        echo "File $file"
                        rm -rf jobs/run_${file}.sh
                        echo "#!/bin/bash

             python $SCRIPT -f $INPUT_DIR/$file -o $PATH_OUTPUT -c $CONFIG" >> jobs/run_${file}.sh

                    done
                done
            else
                INPUT_DIR=${BASEDIR}/${PROD}/${particle}/${zenith}/${pointing}/
                PATH_OUTPUT=/fefs/aswg/workspace/maria.bernardos/LSTanalysis/full_analysis/data/mc/DL1/${PROD}/${particle}/${zenith}/${pointing}/
                mkdir -p ${PATH_OUTPUT}
                for file in $(ls ${INPUT_DIR}/ | grep simtel); do
                    echo "File $file"
                    rm -rf jobs/run_${file}.sh
                    echo "#!/bin/bash

             python $SCRIPT -f $INPUT_DIR/$file -o $PATH_OUTPUT -c $CONFIG" >> jobs/run_${file}.sh

                done
            fi
        done
    done
done
chmod gu+x jobs/run_*
