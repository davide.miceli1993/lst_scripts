#!/bin/bash

TYPE='' #'_addxy' #'_kurtosis'

DATE=20210812
DATADIR='/fefs/aswg/workspace/maria.bernardos/LSTanalysis/full_analysis/data/real/DL2/'
WORKDIR='/fefs/aswg/workspace/maria.bernardos/LSTanalysis/RSOph/'
INPUT_DIRS="${DATADIR}/${DATE}/v0.7.3only_main_island/tailcut84/merged/"
echo $INPUT_DIRS

for input_dir in $INPUT_DIRS; do
    #for i in $(ls $input_dir | grep 202); do
    mkdir -p $WORKDIR/dl2/results/v0.7.3/${DATE}
        for file in $(ls $input_dir/ | grep dl2); do
            echo "File $file"
	    rm -rf jobs/run_store_all_results_${file}.sh

	    echo "#!/bin/bash
#SBATCH -N 3
#SBATCH --mem 100000
ulimit -l unlimited
ulimit -s unlimited
ulimit -a

            python $WORKDIR/scripts/store_all_results_src_independent_custom.py -f $input_dir/$file -o $WORKDIR//dl2/results/v0.7.3/${DATE}" >> jobs/run_store_all_results_${file}.sh


            #for i in $(ls | grep run_store); do sbatch ./$i ; done
	done
        chmod gu+x jobs/run_store_all_results_*
done
