#!/bin/bash

DATES=( 20210809 20210810 20210812 20210813 20210814 20210815 20210830 20210829 20210828 20210827 20210902 20210901 )
#DATES=( 20210813 )
SUFFIX="only_main_island"
PATH_MODELS="/fefs/aswg/workspace/maria.bernardos/LSTanalysis/full_analysis/models/"
SCRIPT=/fefs/aswg/workspace/maria.bernardos/GitHub/cta-lstchain/lstchain/scripts/lstchain_data_r0_to_dl1.py
BASEDIR="/fefs/aswg/data/real/R0/"
VERS="v0.7.3"
TAILCUT="tailcut84"
CONFIG=$PATH_MODELS/${SUFFIX}/lstchain_standard_config_tailcut84.json


for DATE in "${DATES[@]}"
do
    DATE_FORMATTED=$(date -d ${DATE} +%y_%m_%d)
    CALDIR=/fefs/aswg/data/real/calibration/${DATE}/${VERS}/
    PEDESTAL=$(ls ${CALDIR}/drs4_pedestal*)
    CALIB=$(ls ${CALDIR}/calibration*)
    TIME_CALIB=$(ls ${CALDIR}/time_calibration*.hdf5)
    POINTING_FILE=/fefs/aswg/data/real/monitoring/DrivePositioning/drive_log_${DATE_FORMATTED}.txt
    RUN_SUMMARY=/fefs/aswg/data/real/monitoring/RunSummary/RunSummary_${DATE}.ecsv
    INPUT_DIR=${BASEDIR}/${DATE}/
    PATH_OUTPUT=/fefs/aswg/workspace/maria.bernardos/LSTanalysis/full_analysis/data/real/DL1/${DATE}/$VERS$SUFFIX/$TAILCUT
    mkdir -p ${PATH_OUTPUT}
    for file in $(ls ${INPUT_DIR}/ | grep LST-1.1); do
        echo "File $file"
        rm -rf jobs/test/run_${file}.sh

        echo "#!/bin/bash
#SBATCH -N 1
#SBATCH --mem 100000
ulimit -l unlimited
ulimit -s unlimited
ulimit -a
       python $SCRIPT -f $INPUT_DIR$file -o $PATH_OUTPUT -c $CONFIG --pedestal-file $PEDESTAL --calibration-file $CALIB --time-calibration-file $TIME_CALIB --pointing $POINTING_FILE -r $RUN_SUMMARY &"  >> jobs/test/run_${file}.sh
    done
done

chmod gu+x jobs/test/run_*
