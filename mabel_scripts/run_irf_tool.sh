#!/bin/bash

SUFFIX=tuned_dynamic_cleaning_source_independent
BASEDIR=/fefs/aswg/workspace/maria.bernardos/LSTanalysis/full_analysis/data/mc/DL2/20200629_prod5_trans_80_${SUFFIX}/
GAMMAS=${BASEDIR}/gamma/gamma_20200629_prod5_trans_80_tuned_dynamic_cleaning_training.h5
PROTONS=${BASEDIR}/proton/proton_20200629_prod5_trans_80_tuned_dynamic_cleaning_testing.h5
ELECTRONS=${BASEDIR}/electron/electron_20200629_prod5_trans_80_tuned_dynamic_cleaning_training.h5
SCRIPT=/fefs/aswg/workspace/maria.bernardos/GitHub/cta-lstchain/lstchain/tools/lstchain_create_irf_files.py
CUTS=( "crabcut" )

for cut in "${CUTS[@]}"
do
    CONFIG=/fefs/aswg/workspace/maria.bernardos/LSTanalysis/full_analysis/scripts/irf_tool_config${cut}.json
    OUTPUT=/fefs/aswg/workspace/maria.bernardos/LSTanalysis/full_analysis/data/mc/IRF/${SUFFIX}/irf_${SUFFIX}${cut}.fits.gz

    #srun python ${SCRIPT} --point-like --source-dep -g $GAMMAS -p $PROTONS -e $ELECTRONS --overwrite -o $OUTPUT --config $CONFIG &
srun python ${SCRIPT} --point-like -g $GAMMAS -p $PROTONS -e $ELECTRONS --overwrite -o $OUTPUT --config $CONFIG &
done
