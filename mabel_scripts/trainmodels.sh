#!/bin/bash

GAMMA='/fefs/aswg/workspace/maria.bernardos/LSTanalysis/full_analysis/data/mc/DL1/20200629_prod5_trans_80_tuned_dynamic_cleaning/gamma-diffuse/zenith_40deg/south_pointing/gamma-diffuse_20200629_prod5_trans_80_tuned_dynamic_cleaning_training.h5'

PROTON='/fefs/aswg/workspace/maria.bernardos/LSTanalysis/full_analysis/data/mc/DL1/20200629_prod5_trans_80_tuned_dynamic_cleaning/proton/zenith_40deg/south_pointing/proton_20200629_prod5_trans_80_tuned_dynamic_cleaning_training.h5'

OUTPUT='/fefs/aswg/workspace/maria.bernardos/LSTanalysis/full_analysis/models/tuned_dynamic_cleaning_source_independent/'

#tuned
#CONFIG=$OUTPUT/lstchain_tailcuts_8_4.json
#tuned_reduced
#CONFIG=$OUTPUT/lstchain_standard_config_v073_Tel1_tail_8_4_MC_config_reduce_features_skewness.json
#tuned_main_island
#CONFIG=$OUTPUT/lstchain_tailcuts_8_4_main_island.json
#tuned_main_island_reduced
#CONFIG='/fefs/aswg/workspace/maria.bernardos/LSTanalysis/full_analysis/models/only_main_island/lstchain_standard_config_tailcut84.json'

#CONFIG='/fefs/aswg/workspace/maria.bernardos/GitHub/cta-lstchain/lstchain/data/lstchain_src_dep_config.json'
CONFIG='/fefs/aswg/workspace/maria.bernardos/GitHub/cta-lstchain/lstchain/data/lstchain_standard_config.json'

SCRIPT='/fefs/aswg/workspace/maria.bernardos//GitHub/cta-lstchain/lstchain/scripts/lstchain_mc_trainpipe.py'

LOGFILE=$OUTPUT/srun_log.txt
#Run script
echo $OUTPUT
srun -p long --mem=50g -o $LOGFILE python $SCRIPT --fg=$GAMMA --fp=$PROTON -o $OUTPUT -c $CONFIG &
#srun -o $LOGFILE python $SCRIPT --fg=$GAMMA --fp=$PROTON -o $OUTPUT -c $CONFIG &
