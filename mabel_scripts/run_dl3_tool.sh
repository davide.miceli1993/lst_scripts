#!/bin/bash
#Script to Run the dl3_tool over a set of DL2 REAL data.
#Change paths according to your system

#Some variables for retrieving the input files
DATES=( 20201117 20201118 20201120 ) #Dates to analyze

VERS="v0.7.3" #lstchain version
TAILCUT="tailcut84" #tailcut
BASEDIR="/fefs/aswg/workspace/maria.bernardos/LSTanalysis/full_analysis/data/real/DL2/" #Base directory where to find the dl2 data

#Variables for the output directory
SUFFIX="tuned_dynamic_cleaning_source_independent" #Optional suffix
CUT="crabcutloosest" #Name of the cut applied to data ( nocuts, softcut, hardcut, hardestcut )

#IRF file
IRF="/fefs/aswg/workspace/maria.bernardos/LSTanalysis/full_analysis/data/mc/IRF/${SUFFIX}/irf_${SUFFIX}crabcut.fits.gz"

#Configuration file)
CONFIG="/fefs/aswg/workspace/maria.bernardos/LSTanalysis/full_analysis/scripts/config_dl3_tool${CUT}.json"

#Source to analyze
#SRCNAME="RSOph"
#SRCRA="267.554deg"
#SRCDEC="−6.707deg" #CAREFUL, USE TRUE MINUS, NOT HYPHEN MINUS!!!- −
SRCNAME="Crab"
SRCRA="83.63308333deg"
SRCDEC="22.0145deg"

#Script (in ./cta-lstchain/tools/lstchain_create_dl3_file.py)

SCRIPT="/fefs/aswg/workspace/maria.bernardos/GitHub/cta-lstchain/lstchain/tools/lstchain_create_dl3_file.py"

for date in "${DATES[@]}" #Run over dates
do
    #Define and create an output path for each date
    OUTPUT_PATH="/fefs/aswg/workspace/maria.bernardos/LSTanalysis/full_analysis/data/real/DL3/$date/$VERS$SUFFIX/"${TAILCUT}${CUT}
    `mkdir -p $OUTPUT_PATH`

    FILES=`ls $BASEDIR/$date/${VERS}${SUFFIX}/$TAILCUT/merged/*.h5`
    # Run over the merged run files
    mkdir -p jobs/${SUFFIX}/dl3/${date}
    for f in $FILES
    do
        b=$(basename $f)
        run=${b:13:-3}
        if [[ $run != *"."* ]]; then
            FILE=$BASEDIR/$date/$VERS${SUFFIX}/$TAILCUT/merged/dl2_LST-1.Run$run.h5
            #Execute the script. You can remove the "srun --mem=20g -o out.txt" part if you don't want to use the IT cluster
            #srun --mem=20g -o out.txt python $SCRIPT -d $FILE -o $OUTPUT_PATH --input-irf $IRF --source-name $SRCNAME --source-ra $SRCRA --source-dec $SRCDEC --config $CONFIG --overwrite &
            echo "File $b"
            rm -rf jobs/${SUFFIX}/dl3/${date}/run_store_all_results_${run}${CUT}.sh

            echo "#!/bin/bash
#SBATCH -N 3
#SBATCH --mem 100000
ulimit -l unlimited
ulimit -s unlimited
ulimit -a

            python $SCRIPT -d $FILE -o $OUTPUT_PATH --input-irf $IRF --source-name $SRCNAME --source-ra $SRCRA --source-dec $SRCDEC --config $CONFIG --overwrite " >> jobs/${SUFFIX}/dl3/${date}/run_store_all_results_${run}${CUT}.sh
            chmod gu+x jobs/${SUFFIX}/dl3/${date}/run_store_all_results_*
        fi
    done
done
