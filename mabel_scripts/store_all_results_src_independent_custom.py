import sys, os
import numpy as np
import argparse

from gammapy.stats import WStatCountsStatistic

from lstchain.io.io import dl2_params_lstcam_key, dl2_params_src_dep_lstcam_key
from lstchain.io import get_dataset_keys

from astropy.table import Table
import pandas as pd

import astropy.units as u
import matplotlib.pyplot as plt
from astropy.coordinates import ICRS, Galactic, FK4, FK5

from numpy import save

from lstchain.reco.utils import (get_effective_time, radec_to_camera)
from ctapipe.containers import EventType
from lstchain.reco.utils import compute_theta2, extract_source_position

from astropy.coordinates import AltAz, SkyCoord, EarthLocation
import astropy.units as u
from astropy.time import Time
from ctapipe.coordinates import CameraFrame


parser = argparse.ArgumentParser(description="DL1 to DL2")


# Required arguments
parser.add_argument('--input-file', '-f', type=str,
                    dest='input_file',
                    help='path to a DL1 HDF5 file',
                    default=None, required=True)

# Optional arguments
parser.add_argument('--output-dir', '-o', action='store', type=str,
                     dest='output_dir',
                     help='Path where to store the reco dl2 events',
                     default='./dl2_data')

args = parser.parse_args()

GAMMANESS_LIST=[0., 0.05, 0.1, 0.15, 0.2, 0.25, 0.3, 0.35, 0.4, 0.45, 0.5, 0.6, 0.7, 0.8, 0.85, 0.9, 0.95]
INTENSITY_LIST=[0, 50, 100, 200]
#GAMMANESS_LIST=[0.3,0.5, 0.6, 0.7, 0.9]

emin_sensitivity = 0.01 * u.TeV  # mc_par_g['emin']
emax_sensitivity =  10 * u.TeV  # mc_par_g['emax']
n_bins_energy=15
#emax_sensitivity =  40 * u.TeV  # mc_par_g['emax']
#n_bins_energy=20


log_energy = np.linspace(np.log10(emin_sensitivity.to_value()),
                     np.log10(emax_sensitivity.to_value()),
                     n_bins_energy + 1)

def extract_source_position_from_coord(
    data, coord, equivalent_focal_length=28 * u.m
):
    """
    Extract source position from data
    Parameters:
    -----------
    pandas.DataFrame data: input data
    str observed_source_name: Name of the observed source
    astropy.units.m equivalent_focal_length: Equivalent focal length of a telescope
    Returns:
    --------
    2D array of coordinates of the source in form [(x),(y)] in astropy.units.m
    """

    obstime = pd.to_datetime(data["dragon_time"], unit="s")
    pointing_alt = u.Quantity(data["alt_tel"], u.rad, copy=False)
    pointing_az = u.Quantity(data["az_tel"], u.rad, copy=False)
    source_pos_camera = radec_to_camera(
        coord,
        obstime,
        pointing_alt,
        pointing_az,
        focal=equivalent_focal_length,
    )
    source_position = [source_pos_camera.x, source_pos_camera.y]
    return source_position

def main():

    os.makedirs(args.output_dir, exist_ok=True)
    output_file_base = os.path.join(args.output_dir,
                                    os.path.basename(os.path.splitext(args.input_file)[0]))

    data_dl3 = pd.read_hdf(args.input_file, key=dl2_params_lstcam_key)
    obstime_real = get_effective_time(data_dl3)[0]

    gammaness = np.array(data_dl3.gammaness)
    log_reco_energy = np.array(data_dl3.log_reco_energy)

    coords = ["17 50 13.1592776879 -06 42 28.481553668"]
    coordinates = SkyCoord(coords, frame=ICRS, unit=(u.hourangle, u.deg))
    #true_source_position = extract_source_position(data_dl3, 'Crab Nebula')
    true_source_position = extract_source_position_from_coord(data_dl3, coordinates)
    #print('true_source_position', true_source_position)
    off_source_position = [element * -1 for element in true_source_position]

    theta2_on = np.array(compute_theta2(data_dl3, true_source_position))
    theta2_off = np.array(compute_theta2(data_dl3, off_source_position))
    #print('theta2_on', theta2_on)
    leakage_intensity_width_2 = np.array(data_dl3.leakage_intensity_width_2)
    intensity = np.array(data_dl3.intensity)
    alt = np.array(data_dl3.alt_tel)
    event_id = np.array(data_dl3.event_id)
    wl = np.array(data_dl3.wl)
    event_type = np.array(data_dl3.event_type)

    intensity_cut_high = 1e10
    alt_min = 0 * np.pi / 180 # rad
    wl_cut = 0.0

    fig, ax = plt.subplots(figsize = (12, 8))
    for intensity_cut in INTENSITY_LIST:
        print('intensity_cut', intensity_cut)
        for gammaness_cut in GAMMANESS_LIST:
            print('gammaness_cut', gammaness_cut)
            histogram_all_energies_on = []
            histogram_all_energies_off = []

            for i in range(n_bins_energy):  # binning in energy
                #print(f'******** Energy bin between {10**log_energy[i]} and {10**log_energy[i + 1]} TeV *****')

                condition_theta2_off = (log_reco_energy < log_energy[i+1]) \
                                          & (log_reco_energy >= log_energy[i]) \
                                          & (gammaness > gammaness_cut) \
                                          & (intensity > intensity_cut) \
                                          & (intensity < intensity_cut_high) \
                                          & (alt > alt_min) \
                                          & (wl > wl_cut) \
                                          & (event_type != EventType.FLATFIELD.value)\
                                          & (event_type != EventType.SKY_PEDESTAL.value)\
                                          & (leakage_intensity_width_2 < 0.2)
                hist, bin_edges = np.histogram(theta2_off[(condition_theta2_off)],
                                               density=False, bins=100, range=(0, 1))
                histogram_all_energies_off.append(hist)


                condition_theta2_on = (log_reco_energy < log_energy[i+1]) \
                                     & (log_reco_energy >= log_energy[i]) \
                                     & (gammaness > gammaness_cut) \
                                     & (intensity > intensity_cut) \
                                     & (intensity < intensity_cut_high) \
                                     & (alt > alt_min) \
                                     & (wl > wl_cut) \
                                     & (event_type != EventType.FLATFIELD.value)\
                                     & (event_type != EventType.SKY_PEDESTAL.value)\
                                     & (leakage_intensity_width_2 < 0.2)

                hist, bin_edges = np.histogram(theta2_on[(condition_theta2_on)],
                                               density=False, bins=100, range=(0, 1))
                print(f'gammaness cut {gammaness_cut}, histogram {hist}')
                histogram_all_energies_on.append(hist)


            histogram_all_energies_on = np.array(histogram_all_energies_on)
            df = pd.DataFrame(histogram_all_energies_on.T, columns=log_energy[:-1].round(2))
            df.to_hdf(f'{output_file_base}_gammaness{gammaness_cut}_intensity{intensity_cut}_on.h5', key='df', mode='w')

            histogram_all_energies_off = np.array(histogram_all_energies_off)
            df = pd.DataFrame(histogram_all_energies_off.T, columns=log_energy[:-1].round(2))
            df.to_hdf(f'{output_file_base}_gammaness{gammaness_cut}_intensity{intensity_cut}_off.h5', key='df', mode='w')
            save(f'{output_file_base}_effective_time.npy', obstime_real.to_value())

if __name__ == '__main__':
    main()
