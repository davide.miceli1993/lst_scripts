import argparse
import sys, os
import numpy as np
import argparse

from gammapy.stats import WStatCountsStatistic

from lstchain.io.io import dl2_params_lstcam_key, dl2_params_src_dep_lstcam_key
from lstchain.io import get_dataset_keys

from astropy.table import Table
import pandas as pd

import astropy.units as u
import matplotlib.pyplot as plt

from numpy import save

from lstchain.reco.utils import get_effective_time
from ctapipe.containers import EventType

parser = argparse.ArgumentParser(description="DL1 to DL2")


# Required arguments
parser.add_argument('--input-file', '-f', type=str,
                    dest='input_file',
                    help='path to a DL1 HDF5 file',
                    default=None, required=True)

# Optional arguments
parser.add_argument('--output-dir', '-o', action='store', type=str,
                     dest='output_dir',
                     help='Path where to store the reco dl2 events',
                     default='./dl2_data')

args = parser.parse_args()

GAMMANESS_LIST=[0.05, 0.1, 0.15, 0.2, 0.25, 0.3, 0.35, 0.4, 0.45, 0.5, 0.6, 0.7, 0.8, 0.85, 0.9, 0.95]
#GAMMANESS_LIST=[0.3, 0.6, 0.7, 0.8, 0.85, 0.9, 0.95]
#GAMMANESS_LIST=[0.6,0.7, 0.8, 0.85, 0.86, 0.87, 0.88, 0.89, 0.9, 0.95]
#GAMMANESS_LIST=[0.85, 0.86, 0.87, 0.88, 0.89]

ALPHA_LIST=[6, 8, 10, 12, 15, 20]
INTENSITY_LIST=[0, 50, 100, 200]

emin_sensitivity = 0.01 * u.TeV  # mc_par_g['emin']
emax_sensitivity =  100 * u.TeV  # mc_par_g['emax']
n_bins_energy=15
#emax_sensitivity =  40 * u.TeV  # mc_par_g['emax']
#n_bins_energy=20

log_energy = np.linspace(np.log10(emin_sensitivity.to_value()),
                     np.log10(emax_sensitivity.to_value()),
                     n_bins_energy + 1)


def main():

    os.makedirs(args.output_dir, exist_ok=True)
    output_file_base = os.path.join(args.output_dir,
                                    os.path.basename(os.path.splitext(args.input_file)[0]))

    data_dl3 = pd.read_hdf(args.input_file, key=dl2_params_lstcam_key)
    obstime_real = get_effective_time(data_dl3)[0]
    data_dl3_src_dep = pd.read_hdf(args.input_file, key=dl2_params_src_dep_lstcam_key)

    #phases = np.array(data_dl3.phase)
    #phases_corrected = phases * (phases > 0) + (phases + 1) * (phases < 0)

    alpha_on = np.array(data_dl3_src_dep['(\'on\', \'alpha\')'])
    log_reco_energy_on = np.array(data_dl3_src_dep['(\'on\', \'log_reco_energy\')'])
    gammaness_on = np.array(data_dl3_src_dep['(\'on\', \'gammaness\')'])

    alpha_off_090 = np.array(data_dl3_src_dep['(\'off_090\', \'alpha\')'])
    log_reco_energy_off_090 = np.array(data_dl3_src_dep['(\'off_090\', \'log_reco_energy\')'])
    gammaness_off_090 = np.array(data_dl3_src_dep['(\'off_090\', \'gammaness\')'])

    alpha_off_180 = np.array(data_dl3_src_dep['(\'off_180\', \'alpha\')'])
    log_reco_energy_off_180 = np.array(data_dl3_src_dep['(\'off_180\', \'log_reco_energy\')'])
    gammaness_off_180 = np.array(data_dl3_src_dep['(\'off_180\', \'gammaness\')'])

    alpha_off_270 = np.array(data_dl3_src_dep['(\'off_270\', \'alpha\')'])
    log_reco_energy_off_270 = np.array(data_dl3_src_dep['(\'off_270\', \'log_reco_energy\')'])
    gammaness_off_270 = np.array(data_dl3_src_dep['(\'off_270\', \'gammaness\')'])

    leakage_intensity_width_2 = np.array(data_dl3.leakage_intensity_width_2)
    intensity = np.array(data_dl3.intensity)
    alt = np.array(data_dl3.alt_tel)
    event_id = np.array(data_dl3.event_id)
    wl = np.array(data_dl3.wl)
    ucts_event_type = np.array(data_dl3.ucts_trigger_type)
    event_type = np.array(data_dl3.event_type)

    intensity_cut_high = 1e10
    #alt_min = 55 * np.pi / 180 # rad
    alt_min=0
    wl_cut = 0.0

    fig, ax = plt.subplots(figsize = (12, 8))
    for intensity_cut in INTENSITY_LIST:
        print('intensity_cut', intensity_cut)
        for gammaness_cut in GAMMANESS_LIST:
            print('gammaness_cut', gammaness_cut)
            histogram_all_energies_on = []
            histogram_all_energies_off_090 = []
            histogram_all_energies_off_180 = []
            histogram_all_energies_off_270 = []

            for i in range(n_bins_energy):  # binning in energy
                #print(f'******** Energy bin between {10**log_energy[i]} and {10**log_energy[i + 1]} TeV *****')

                condition_alpha_off_090 = (log_reco_energy_off_090 < log_energy[i+1]) \
                                          & (log_reco_energy_off_090 >= log_energy[i]) \
                                          & (gammaness_off_090 > gammaness_cut) \
                                          & (intensity > intensity_cut) \
                                          & (intensity < intensity_cut_high) \
                                          & (alt > alt_min) \
                                          & (wl > wl_cut) \
                                          & (event_type != EventType.FLATFIELD.value)\
                                          & (event_type != EventType.SKY_PEDESTAL.value)\
                                          & (leakage_intensity_width_2 < 0.2)
                hist, bin_edges = np.histogram(alpha_off_090[(condition_alpha_off_090)],
                                               density=False, bins=90, range=(0,90))
                histogram_all_energies_off_090.append(hist)


                condition_alpha_off_180 = (log_reco_energy_off_180 < log_energy[i+1]) \
                                          & (log_reco_energy_off_180 >= log_energy[i]) \
                                          & (gammaness_off_180 > gammaness_cut) \
                                          & (intensity > intensity_cut) \
                                          & (intensity < intensity_cut_high) \
                                          & (alt > alt_min) \
                                          & (wl > wl_cut) \
                                          & (event_type != EventType.FLATFIELD.value)\
                                          & (event_type != EventType.SKY_PEDESTAL.value)\
                                          & (leakage_intensity_width_2 < 0.2)

                hist, bin_edges = np.histogram(alpha_off_180[(condition_alpha_off_180)],
                                               density=False, bins=90, range=(0,90))
                histogram_all_energies_off_180.append(hist)

                condition_alpha_off_270 = (log_reco_energy_off_270 < log_energy[i+1]) \
                                          & (log_reco_energy_off_270 >= log_energy[i]) \
                                          & (gammaness_off_270 > gammaness_cut) \
                                          & (intensity > intensity_cut) \
                                          & (intensity < intensity_cut_high) \
                                          & (alt > alt_min) \
                                          & (wl > wl_cut) \
                                          & (event_type != EventType.FLATFIELD.value)\
                                          & (event_type != EventType.SKY_PEDESTAL.value)\
                                          & (leakage_intensity_width_2 < 0.2)

                hist, bin_edges = np.histogram(alpha_off_270[(condition_alpha_off_270)],
                                               density=False, bins=90, range=(0,90))
                histogram_all_energies_off_270.append(hist)

                condition_alpha_on = (log_reco_energy_on < log_energy[i+1]) \
                                     & (log_reco_energy_on >= log_energy[i]) \
                                     & (gammaness_on > gammaness_cut) \
                                     & (intensity > intensity_cut) \
                                     & (intensity < intensity_cut_high) \
                                     & (alt > alt_min) \
                                     & (wl > wl_cut) \
                                     & (event_type != EventType.FLATFIELD.value)\
                                     & (event_type != EventType.SKY_PEDESTAL.value)\
                                     & (leakage_intensity_width_2 < 0.2)

                hist, bin_edges = np.histogram(alpha_on[(condition_alpha_on)],
                                               density=False, bins=90, range=(0,90))
                print(f'gammaness cut {gammaness_cut}, histogram {hist}')
                histogram_all_energies_on.append(hist)
                """
                #***** PHASES *****
                for alpha_cut in ALPHA_LIST:
                    condition_phase = [(alpha_on < alpha_cut) & condition_alpha_on]
                    phases_after_cuts = (phases_corrected[condition_phase])
                    save(f'{output_file_base}_gammaness{gammaness_cut}_alpha{alpha_cut}_intensity{intensity_cut}'\
                         f'_energy_min{log_energy[i]:.1f}_energy_max{log_energy[i+1]:.1f}.npy', phases_after_cuts)
                    event_id_after_cuts = (event_id[condition_phase])
                    save(f'{output_file_base}_gammaness{gammaness_cut}_alpha{alpha_cut}_intensity{intensity_cut}'\
                         f'_energy_min{log_energy[i]:.1f}_energy_max{log_energy[i+1]:.1f}_event_ids.npy', event_id_after_cuts)
                """
            histogram_all_energies_on = np.array(histogram_all_energies_on)
            df = pd.DataFrame(histogram_all_energies_on.T, columns=log_energy[:-1].round(2))
            df.to_hdf(f'{output_file_base}_gammaness{gammaness_cut}_intensity{intensity_cut}_on.h5', key='df', mode='w')

            histogram_all_energies_off_090 = np.array(histogram_all_energies_off_090)
            df = pd.DataFrame(histogram_all_energies_off_090.T, columns=log_energy[:-1].round(2))
            df.to_hdf(f'{output_file_base}_gammaness{gammaness_cut}_intensity{intensity_cut}_off_090.h5', key='df', mode='w')

            histogram_all_energies_off_180 = np.array(histogram_all_energies_off_180)
            df = pd.DataFrame(histogram_all_energies_off_180.T, columns=log_energy[:-1].round(2))
            df.to_hdf(f'{output_file_base}_gammaness{gammaness_cut}_intensity{intensity_cut}_off_180.h5', key='df', mode='w')

            histogram_all_energies_off_270 = np.array(histogram_all_energies_off_270)
            df = pd.DataFrame(histogram_all_energies_off_270.T, columns=log_energy[:-1].round(2))
            df.to_hdf(f'{output_file_base}_gammaness{gammaness_cut}_intensity{intensity_cut}_off_270.h5', key='df', mode='w')
            save(f'{output_file_base}_effective_time.npy', obstime_real.to_value())

if __name__ == '__main__':
    main()
