#!/bin/bash


TYPES=( "gamma" )
SUFFIX="tuned_dynamic_cleaning"
PATH_MODELS="/fefs/aswg/workspace/maria.bernardos/LSTanalysis/full_analysis/models/"
SCRIPT=/fefs/aswg/workspace/maria.bernardos/GitHub/cta-lstchain/lstchain/scripts/lstchain_dl1ab.py
BASEDIR="/fefs/aswg/data/mc/DL1/"
VERS="20210506_v0.7.3_prod5_trans_80_zen40deg_local_tailcut_8_4"
VERS="20210517_v0.7.3_prod5_trans_80_local_tailcut_8_4"
TAILCUT="tailcut84"
ZENITHS=( "zenith_40deg" )
POINTINGS=( "north_pointing" )
PROD="20200629_prod5_trans_80"
OFFSETS=( "off0.4deg" )
CONFIG=$PATH_MODELS/${SUFFIX}/lstchain_dl1ab_tune_MC_to_Crab_config.json
SET="testing"

for particle in "${TYPES[@]}"
do
    for zenith in "${ZENITHS[@]}"
    do
        for pointing in "${POINTINGS[@]}"
        do
            if [ $particle == "gamma" ]
            then
                for offset in "${OFFSETS[@]}"
                do
                    INPUT_DIR=${BASEDIR}/${PROD}/${particle}/${zenith}/${pointing}/${VERS}/${offset}/${SET}/
                    PATH_OUTPUT=/fefs/aswg/workspace/maria.bernardos/LSTanalysis/full_analysis/data/mc/DL1/${PROD}_${SUFFIX}/${particle}/${zenith}/${pointing}/${offset}/${SET}/
                    #SI QUIERO HACER MAS DE UN OFFSET TENGO QUE PONER TODO DENTRO DE ESTE LOOP
                done
            else
                    INPUT_DIR=${BASEDIR}/${PROD}/${particle}/${zenith}/${pointing}/${VERS}/${SET}/
                    PATH_OUTPUT=/fefs/aswg/workspace/maria.bernardos/LSTanalysis/full_analysis/data/mc/DL1/${PROD}_${SUFFIX}/${particle}/${zenith}/${pointing}/${SET}/
            fi
            mkdir -p ${PATH_OUTPUT}
            mkdir -p jobs/${SUFFIX}/dl1ab_mc/

            for file in $(ls ${INPUT_DIR}/ | grep .h5); do
                if [ ! -f $PATH_OUTPUT$file ]
                then
                    echo "File $file"
                    rm -rf jobs/${SUFFIX}/dl1ab_mc/run_${file}.sh

                    echo "#!/bin/bash
#SBATCH -N 1
#SBATCH --mem 50g
ulimit -l unlimited
ulimit -s unlimited
ulimit -a
       python $SCRIPT -f $INPUT_DIR$file -o $PATH_OUTPUT$file -c $CONFIG --no-image True &"  >> jobs/${SUFFIX}/dl1ab_mc/run_${file}.sh
                fi
            done
        done
    done
done
chmod gu+x jobs/${SUFFIX}/dl1ab_mc/run_*
