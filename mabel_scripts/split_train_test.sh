#!/bin/bash

TYPES=( "gamma-diffuse" "proton" "gamma" "electron" )
OFFSETS=( "off0.0deg" "off0.4deg" )
#TYPES=("proton")
BASEDIR=/fefs/aswg/workspace/maria.bernardos/LSTanalysis/full_analysis/data/mc/DL1/
ZENITHS=( "zenith_40deg" )
POINTINGS=( "south_pointing" )
PROD="20200629_prod5_trans_80"
SUFFIX='only_main_island'

for particle in "${TYPES[@]}"
do
    for zenith in "${ZENITHS[@]}"
    do
        for pointing in "${POINTINGS[@]}"
        do
            if [ $particle == "gamma" ]
            then
                for offset in "${OFFSETS[@]}"
                do
                    path_files=${BASEDIR}/${PROD}/${particle}/${zenith}/${pointing}/${offset}
                    test_dir=$path_files'/'testing/
                    train_dir=$path_files'/'training/
                    if [ ! -d test_dir ]
                    then
                        `mkdir -p $test_dir`
                    fi
                    if [ ! -d train_dir ]
                    then
                        `mkdir -p $train_dir`
                    fi

                    files=$(ls $path_files'/'*.h5)
                    counter=0
                    split=2
                    for file in $files
                    do
                        let rest=$(expr $counter % $split)
                        if [[ $rest == 0 ]]
                        then
                            `mv $file $test_dir`
                            echo "goes to testing: "$file
                        else
                            `mv $file $train_dir`
                            echo "goes to tarining: "$file
                        fi
                        let counter++
                    done
                done
            else
                path_files=${BASEDIR}/${PROD}/${particle}/${zenith}/${pointing}/
                test_dir=$path_files'/'testing/
                train_dir=$path_files'/'training/
                if [ ! -d test_dir ]
                then
                    `mkdir -p $test_dir`
                fi
                if [ ! -d train_dir ]
                then
                    `mkdir -p $train_dir`
                fi

                files=$(ls $path_files'/'*.h5)
                counter=0
                split=2
                for file in $files
                do
                    let rest=$(expr $counter % $split)
                    if [[ $rest == 0 ]]
                    then
                        echo "goes to testing: "$file
                        `mv $file $test_dir`
                    else
                        echo "goes to training: "$file
                        `mv $file $train_dir`
                    fi
                    let counter++
                done
            fi
        done
    done
done
