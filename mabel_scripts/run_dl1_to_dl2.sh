#!/bin/bash

#DATES=( 20201120 )
#DATES=( 20210809 20210810 20210812 20210813 20210814 20210815 20210830 20210829 20210828 20210827 20210902 20210901 )
DATES=( 20201118 )
SUFFIX="tuned_dynamic_cleaning"
PATH_MODELS="/fefs/aswg/workspace/maria.bernardos/LSTanalysis/full_analysis/models/${SUFFIX}_source_independent"
SCRIPT="/fefs/aswg/workspace/maria.bernardos/GitHub/cta-lstchain/lstchain/scripts/lstchain_dl1_to_dl2.py"

#CONFIG=/fefs/aswg/workspace/maria.bernardos/LSTanalysis/full_analysis/scripts/lstchain_src_dep_config_crab.json
CONFIG=/fefs/aswg/workspace/maria.bernardos/GitHub/cta-lstchain/lstchain/data/lstchain_standard_config.json

for date in "${DATES[@]}"
do
    INPUT_DIR=/fefs/aswg/workspace/maria.bernardos/LSTanalysis/full_analysis/data/real/DL1/${date}/v0.7.1${SUFFIX}/tailcut84/
    #INPUT_DIR=/fefs/aswg/data/real/DL1/20201120/v0.7.5_test_new_calibration_tree/tailcut84_dynamic_cleaning/
    OUTPUT_DIR=/fefs/aswg/workspace/maria.bernardos/LSTanalysis/full_analysis/data/real/DL2/${date}/v0.7.3${SUFFIX}_source_independent/tailcut84/
    mkdir -p ${OUTPUT_DIR}
    mkdir -p jobs/${SUFFIX}_source_independent/dl2/${date}/
    for file in $(ls ${INPUT_DIR}/ | grep dl1_LST); do
        echo "File $file"
        rm -rf jobs/${SUFFIX}_source_independent/dl2/${date}/run_${file}.sh
        echo "#!/bin/bash
#SBATCH -N 1
#SBATCH --mem 100000
ulimit -l unlimited
ulimit -s unlimited
ulimit -a
        python $SCRIPT -f $INPUT_DIR$file -p $PATH_MODELS -o $OUTPUT_DIR -c $CONFIG" >> jobs/${SUFFIX}_source_independent/dl2/${date}/run_${file}.sh
    done
    chmod gu+x jobs/${SUFFIX}_source_independent/dl2/${date}/run_*
done
