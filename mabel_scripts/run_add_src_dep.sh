#!/bin/bash
DATES=( 20210903 )
#DATES=( 20210809 20210810 20210812 )

#DATES=( 20210812 )
SCRIPT=/fefs/aswg/workspace/maria.bernardos/GitHub/cta-lstchain/lstchain/scripts/lstchain_add_source_dependent_parameters.py
CONFIG=/fefs/aswg/workspace/maria.bernardos/LSTanalysis/full_analysis/scripts/lstchain_src_dep_config_crab.json

for date in "${DATES[@]}"
do
    INPUT_DIR=/fefs/aswg/workspace/maria.bernardos/LSTanalysis/full_analysis/data/real/DL1/${date}/v0.7.3tuned_dynamic_cleaning/tailcut84/
    mkdir -p jobs/tuned_dynamic_cleaning/srcdep/${date}
    for file in $(ls ${INPUT_DIR}/ | grep dl1_LST-1); do
        echo "File $file"
        rm -rf jobs/tuned_dynamic_cleaning/srcdep/${date}/run_add_srcdep_${file}.sh
	echo "#!/bin/bash
#SBATCH -N 1
#SBATCH --mem 100000
#SBATCH -p long
ulimit -l unlimited
ulimit -s unlimited
ulimit -a
        python $SCRIPT -f $INPUT_DIR$file -c $CONFIG" >> jobs/tuned_dynamic_cleaning/srcdep/${date}/run_add_srcdep_${file}.sh
    done
    chmod gu+x jobs/tuned_dynamic_cleaning/srcdep/${date}/run_add_srcdep_*
done
