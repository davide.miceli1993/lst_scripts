# LST_scripts

Collection of scripts to run analysis of LST data at the IT cluster in La Palma.

Some simple bash scripts are also available.

## Resources

Documentation:

- [readthedocs](https://lst-analysis.readthedocs.io/en/latest/index.html)


## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.
