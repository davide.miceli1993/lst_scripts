import pandas as pd
import argparse
import numpy as np
from astropy.time import Time
import os

columns = ['Target', 'Source', 'day', 'hour', 'MJD', 'RA_Obs', 'Dec_Obs', 'obs_time', 'Zenith', 'rate', 'nsb']
df = pd.DataFrame(columns=columns)

parser = argparse.ArgumentParser(description='Generate and update the database')
parser.add_argument('--name', '-n',
                    dest='db', type=str, required=False, default='database.csv',
                    help='Name of the database'
                    )
args = parser.parse_args()

# get the online database
os.system("wget --no-check-certificate -O SeiyaDB.csv 'https://docs.google.com/spreadsheets/d/1S5wcgqldMN3Tt-51IseJOaRnKbDvh1QTc7fgUrCB-mc/export?gid=0&format=csv'")

data = np.loadtxt("SeiyaDB.csv", skiprows=2, delimiter=",", unpack=True, dtype=str)
Nentry = len(data[0])
print("Nentry", Nentry)

newEntry = {}
for i in range(Nentry):
    if (data[21][i] == "TRUE" and data[22][i] == "FALSE" and data[23][i] == "FALSE"):
        newEntry['Target'] = data[5][i]
        newEntry['Source'] = data[6][i]
        newEntry['day'] = int(data[1][i].replace("-", ""))
        newEntry['hour'] = data[2][i] + ":00"
        newEntry['RA_Obs'] = float(data[15][i])
        newEntry['Dec_Obs'] = float(data[16][i])
        t = Time(data[1][i]+"T"+data[2][i] + ":00", format='isot', scale='utc')
        newEntry['MJD'] = t.mjd
        newEntry['obs_time'] = float(data[3][i])
        newEntry['Zenith'] = (float(data[10][i])+float(data[9][i]))/2.
        newEntry['rate'] = float(data[11][i])
        newEntry['nsb'] = float(data[12][i])
        df.loc[data[0][i]] = newEntry
        # df = df.append(newEntry,ignore_index=True)

df["day"] = pd.to_numeric(df["day"])
print(df)
print(df.info())
df.to_csv(args.db)
