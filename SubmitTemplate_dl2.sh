#!/bin/sh
#SBATCH -p short,long
#SBATCH -J jobname
#SBATCH --mem=70g
#SBATCH -N 1
#SBATCH --exclusive
#SBATCH -o logfile

ulimit -l unlimited
ulimit -s unlimited
ulimit -a
