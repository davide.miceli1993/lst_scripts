from astropy.utils import iers
from astropy.utils.data import import_file_to_cache, download_file


WORKING_IERS_A_URL = "https://maia.usno.navy.mil/ser7/finals2000A.all"

path = download_file(WORKING_IERS_A_URL)
import_file_to_cache(iers.IERS_A_URL, path, replace=True)
