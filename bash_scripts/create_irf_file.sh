#!/bin/bash

# Configuration to automatic get MC path, otherwise define your own path
DL2_MC_DIR="/fefs/aswg/data/mc/DL2/20200629_prod5_trans_80"
ZD=40
DIRECTION="south" # north
VERSION="20210416_v0.7.3_prod5_trans_80_local_taicut_8_4" # 20210923_v0.7.5_prod5_trans_80_dynamic_cleaning
GAMMATYPE="point" # diffuse

# fix for typo in MC path
if [ $ZD == 40 ]; then
    VERSION="20210506_v0.7.3_prod5_trans_80_zen40deg_local_tailcut_8_4"
fi

# Get Az from direction
if [ $DIRECTION = "south" ]; then
    AZ=180
elif [ $DIRECTION = "north" ]; then
    AZ=0
else
    exit 1
fi

# make path to the MC files
if [ $GAMMATYPE = "point" ]; then
    MCGAMMA="${DL2_MC_DIR}/gamma/zenith_${ZD}deg/${DIR}_pointing/$PROCESSVER/off${OFF}deg/dl2_gamma_${ZD}deg_${AZ}deg_off${OFF}deg_${PROCESSVER}_t
esting.h5"
    OPTION="--point-like"
elif [ $GAMMATYPE = "diffuse" ]; then
    GAMMA="${DL2_MC_DIR}/gamma-diffuse/zenith_${ZD}deg/${DIRECTION}_pointing/$VERSION/dl2_gamma-diffuse_${ZD}deg_${AZ}deg_${VERSION}_testing.h5"
else
    echo "Data type invalid: $DATATYPE"
    exit 1
fi

PROTON="${DL2_MC_DIR}/proton/zenith_${ZD}deg/${DIRECTION}_pointing/$VERSION/dl2_proton_${ZD}deg_${AZ}deg_${VERSION}_testing.h5"
ELECTRON="${DL2_MC_DIR}/electron/zenith_${ZD}deg/${DIRECTION}_pointing/$VERSION/dl2_electron_${ZD}deg_${AZ}deg_${VERSION}_testing.h5"

# Define output directory
OUTDIR="/fefs/aswg/workspace/alice.donini/Analysis/data/IRF/"
if [ ! -e $OUTDIR ]; then
    mkdir -p $OUTDIR
fi

# If no automatic search, define the full path here and uncomment
#DL2_MC_DIR=/fefs/aswg/workspace/alice.donini/Analysis/data/MC/DL2
#GAMMA=${DL2_MC_DIR}/gamma/gamma_20200629_prod5_trans_80_tuned_dynamic_cleaning_testing.h5
#PROTON=${DL2_MC_DIR}/proton/proton_20200629_prod5_trans_80_tuned_dynamic_cleaning_testing.h5
#ELECTRON=${DL2_MC_DIR}/electron/electron_20200629_prod5_trans_80_tuned_dynamic_cleaning_testing.h5

SCRIPT=/fefs/aswg/workspace/alice.donini/Software/cta-lstchain/lstchain/tools/lstchain_create_irf_files.py
CUTS=( "standard" )

CMD="python $SCRIPT -g $GAMMA -p $PROTON -e $ELECTRON -o"
#Run script
echo "Executing:"
echo $CMD
echo "Output will be saved in:"
echo $OUTDIR

for cut in "${CUTS[@]}"
do
    # To add if you want to use a specific configuration file
    #CONFIG=/fefs/aswg/workspace/maria.bernardos/LSTanalysis/full_analysis/scripts/irf_tool_config_${cut}.json
    OUTPUT=$OUTDIR/irf_${VERSION}_Zd${ZD}deg_${DIRECTION}_off{off}deg_irf_${ZD}_${cut}.fits.gz

    #srun python ${SCRIPT} --point-like --source-dep -g $GAMMAS -p $PROTONS -e $ELECTRONS --overwrite -o $OUTPUT --config $CONFIG &
    srun python ${SCRIPT} -g $GAMMA -p $PROTON -e $ELECTRON -o $OUTPUT --overwrite $OPTION &
done
