#!/bin/bash

# Configuration to automatic get MC path, otherwise define your own path
DL1_MC_DIR="/fefs/aswg/data/mc/DL1/20200629_prod5_trans_80"
ZD=40
DIRECTION="south" # north
VERSION="20210416_v0.7.3_prod5_trans_80_local_taicut_8_4" # 20210923_v0.7.5_prod5_trans_80_dynamic_cleaning

# fix for typo in MC path
if [ $ZD == 40 ]; then
    VERSION="20210506_v0.7.3_prod5_trans_80_zen40deg_local_tailcut_8_4"
fi

# Get Az from direction
if [ $DIRECTION = "south" ]; then
    AZ=180
elif [ $DIRECTION = "north" ]; then
    AZ=0
else
    exit 1
fi

# make path to the MC files
GAMMA="${DL1_MC_DIR}/gamma-diffuse/zenith_${ZD}deg/${DIRECTION}_pointing/$VERSION/dl1_gamma-diffuse_${ZD}deg_${AZ}deg_${VERSION}_training.h5"
PROTON="${DL1_MC_DIR}/proton/zenith_${ZD}deg/${DIRECTION}_pointing/$VERSION/dl1_proton_${ZD}deg_${AZ}deg_${VERSION}_training.h5"

# If no automatic search, define the full path here and uncomment
#DL1_MC_DIR=/fefs/aswg/workspace/alice.donini/Analysis/data/MC/DL1
#GAMMA=${DL1_MC_DIR}/gamma/gamma_20200629_prod5_trans_80_tuned_dynamic_cleaning_training.h5
#PROTON=${DL1_MC_DIR}/proton/proton_20200629_prod5_trans_80_tuned_dynamic_cleaning_testing.h5

# Define output directory
OUTDIR="/fefs/aswg/workspace/alice.donini/Analysis/data/models/source_independent"
if [ ! -e $OUTDIR ]; then
    mkdir -p $OUTDIR
fi

#CONFIG='/fefs/aswg/workspace/alice.donini/Software/cta-lstchain/lstchain/data/lstchain_src_dep_config.json'
CONFIG='/fefs/aswg/workspace/alice.donini/Software/cta-lstchain/lstchain/data/lstchain_standard_config.json'

SCRIPT='/fefs/aswg/workspace/alice.donini/Software/cta-lstchain/lstchain/scripts/lstchain_mc_trainpipe.py'

LOGFILE=$OUTDIR/srun_log.txt

CMD="python $SCRIPT --fg=$GAMMA --fp=$PROTON -o $OUTDIR"
#Run script
echo "Executing:"
echo $CMD
echo "Output will be saved in:"
echo $OUTDIR

srun -p long --mem=50g -o $LOGFILE python $SCRIPT --fg=$GAMMA --fp=$PROTON -o $OUTDIR&
#srun -o $LOGFILE python $SCRIPT --fg=$GAMMA --fp=$PROTON -o $OUTDIR -c $CONFIG &
