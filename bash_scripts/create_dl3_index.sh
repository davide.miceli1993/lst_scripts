#!/bin/bash
#Script to create the DL3 index.
#Change paths according to your system

#Some variables for retrieving the input files
DATES=( 20201120 )
CLEANING="tailcut84" #tailcut
SOURCE="Crab" #name of source
VERSION="v0.9.2" #lstchain version
CUT='standard'

INPUT_DIR="/fefs/aswg/workspace/alice.donini/Analysis/data/" #Base directory where to find the dl3 data

#Script (in ./cta-lstchain/tools/)
SCRIPT="/fefs/aswg/workspace/alice.donini/Software/cta-lstchain/lstchain/tools/lstchain_create_dl3_index_files.py"

for date in "${DATES[@]}"
do
    DIR=$INPUT_DIR/DL3/${SOURCE}/$date/$VERSION/${CLEANING}/
    #srun -o out.txt python $SCRIPT -d $DIR --overwrite &
    srun python $SCRIPT -d $DIR --overwrite &
done
