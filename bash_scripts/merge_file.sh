#!/bin/bash
#Script to merge real data.
#Change paths according to your system

#DATES=( 20201118 20201119 20201120 )
DATES=( 20201120 )
VERSION="v0.9.2"
#SUFFIX="_tuned_main_island"
CLEANING="tailcut84"

INDIR=/fefs/aswg/workspace/alice.donini/Analysis/data/DL2/
SCRIPT="/fefs/aswg/workspace/alice.donini/Software/cta-lstchain/lstchain/scripts/lstchain_merge_hdf5_files.py"

for date in "${DATES[@]}"
do
    DIR=$INDIR/$date/$VERSION/$CLEANING/
    mkdir -p ${DIR}/merged
    FILES=`ls $DIR/dl2*.h5`
    current_run=0000
    for f in $FILES
    do
        b=$(basename $f)
        run=${b:14:-8}
        if [ "$run" -eq "$current_run" ]
        then
            continue
        else
            current_run=$run
            echo $run
            OUTNAME=${DIR}/merged/'dl2_LST-1.Run0'$run'.h5'
            if [ ! -f $OUTNAME ]
            then
                srun -p long -o $DIR/run_${run}_merge.log python $SCRIPT -d $DIR -o $OUTNAME -r $run --smart False &
            fi
        fi
    done
done
