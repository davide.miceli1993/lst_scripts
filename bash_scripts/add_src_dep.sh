#!/bin/bash
#Script to add source dependent parameters to DL1 data.
#Change paths according to your system

#Some variables for retrieving the input files
DATES=( 20201120 )
#DATES=( 20201118 20201119 20201120 )
SOURCE="Crab" #name of source
VERSION="v0.9.2" #lstchain version
CLEANING="tailcut84" #tailcut

INPUT_DIR="/fefs/aswg/workspace/alice.donini/Analysis/data/" #Base directory where to find the dl1 data
SCRIPT=/fefs/aswg/workspace/alice.donini/Software/cta-lstchain/lstchain/scripts/lstchain_add_source_dependent_parameters.py
#CONFIG=/fefs/aswg/workspace/alice.donini/Analysis/config/lstchain_src_dep_config_crab.json

for date in "${DATES[@]}"
do
    DIR=${INPUT_DIR}/DL1/${SOURCE}/${date}/${VERSION}/${CLEANING}/
    mkdir -p jobs/srcdep/${date}
    for file in $(ls ${DIR}/ | grep dl1_LST-1); do
        echo "File $file"
        rm -rf jobs/srcdep/${date}/run_add_srcdep_${file}.sh
    echo "#!/bin/bash
#SBATCH -N 1
#SBATCH --mem=8g
#SBATCH -p long
#SBATCH --exclusive

ulimit -l unlimited
ulimit -s unlimited
ulimit -a
        python $SCRIPT -f $DIR$file" >> jobs/srcdep/${date}/run_add_srcdep_${file}.sh
        #python $SCRIPT -f $DIR$file -c $CONFIG" >> jobs/srcdep/${date}/run_add_srcdep_${file}.sh
        sbatch jobs/srcdep/${date}/run_add_srcdep_${file}.sh
    done
done
