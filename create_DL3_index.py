import os
from utils import (parse_arguments,
                   get_config)

# Argument parser
args = parse_arguments(description="DL3 index maker",
                       add_common=True,
                       add_job=False)

config_file = get_config(args.config)

for night in config_file['dl2_data']['night']:
    # output directory
    dl3_out_dir = config_file['base_dir'] + '/DL3/' + args.source_name + '/' + str(night) + '/' + config_file['dl2_data']['version'] + '/' + config_file['dl2_data']['cleaning']
    if args.outdir is not None:
        dl3_out_dir = args.outdir

    # check if directory exists
    if not (os.path.isdir(dl3_out_dir)):
        raise Exception(f'Output directory {dl3_out_dir} doesn\'t exists.')

    cmd_index = "lstchain_create_dl3_index_files --input-dl3-dir " + dl3_out_dir + " --file-pattern dl3*fits.gz --overwrite"

    if args.verbose:
        print('\n')
        print(cmd_index)
    if not(args.dry):
        os.system(cmd_index)
