import os
from utils import (
    parse_arguments,
    get_config,
    manage_submission,
    )

# Argument parser
args = parse_arguments(description="Train RF",
                       add_job=True
                       )

config_file = get_config(args.config)

# variables definition
off = config_file['offset']
if off not in ('0.0', '0.4'):
    raise ValueError('Offest value is not valid')
Zd = config_file['zenith']
version = config_file['version']
direction = config_file['direction']
gamma_type = config_file['gammatype']

# fix for typo in MC path
if Zd == 40:
    version = '20210506_v0.7.3_prod5_trans_80_zen40deg_local_tailcut_8_4'

# check existance of directories
DL1_mc_dir = config_file['DL1_mc_dir']
if not(os.path.exists(DL1_mc_dir)):
    raise RuntimeError("DL1 MC directory does not exist")

# get Az from direction
if direction == 'south':
    Az = 180
elif direction == 'north':
    Az = 0
else:
    raise ValueError('Variable direction is not valid')

# select MC files
gamma_mc = f'{DL1_mc_dir}/gamma-diffuse/zenith_${Zd}deg/${direction}_pointing/{version}/dl1_gamma-diffuse_{Zd}deg_{Az}deg_{version}_training.h5'
proton_mc = f'{DL1_mc_dir}/proton/zenith_{Zd}deg/{direction}_pointing/{version}/dl1_proton_{Zd}deg_{Az}deg_{version}_training.h5'

if args.verbose:
    print('##################')
    print('MC gamma file selected:')
    print('MC gamma: ', gamma_mc)
    print('MC proton: ', proton_mc)
    print('##################')

# output directory
outdir_path = config_file['output_data_folder']
if args.outdir is not None:
    outdir_path = args.outdir
# check if directory exists, if not, create it
if not (os.path.isdir(outdir_path)):
    try:
        os.makedirs(outdir_path, exist_ok=True)
        print(f'Output directory {outdir_path} created successfully')
    except OSError:
        print(f'Output directory {outdir_path} can not be created')
else:
    print(f'Data will be stored in {outdir_path}')

model_cmd = f'lstchain_mc_trainpipe.py \
--input-file-gamma {gamma_mc} \
--input-file-proton {proton_mc} \
--output-dir {outdir_path}' \
+ ['', ' --overwrite '][args.globber is not None] \
+ ['', f' --config {args.config_analysis}'][args.config_analysis is not None]

# Send interactive, sbatch or dry run
if args.verbose:
    print('\n')
    print(model_cmd)
if not(args.dry):
    if args.submit:
        # create the script that will be submited and return the name of the script in the variable scriptname
        run = 'RF'
        scriptname = manage_submission(args, config_file, model_cmd, run)
        os.system("sbatch "+scriptname)
    else:
        print('Interactive creation of RF model\n')
        os.system(model_cmd)
