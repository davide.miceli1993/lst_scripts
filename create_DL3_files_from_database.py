import os
import pandas as pd
from os.path import join
import datetime
from utils import (
    get_runs_database,
    get_db,
    parse_arguments,
    get_config,
    create_DL2_list,
    manage_submission,
    get_coordinates,
    )

# check that the environment variables are set
#if os.environ.get('CODE_DIR') is None:
#    raise RuntimeError("Set the variable CODE_DIR")

# Argument parser
args = parse_arguments(description="DL2 to DL3 converter",
                       add_job=True,
                       add_run=True,
                       add_dl3=True)

config_file = get_config(args.config)
#database_file = get_db(config_file['db'])

# look into the db and give the run list
#databaseRuns = get_runs_database(args, database_file)

#final_run_list = pd.DataFrame(columns=['path'])
for night in config_file['dl2_data']['night']:
    print(night)
    run_list = create_DL2_list(args, config_file, databaseRuns, str(night))
    final_run_list = pd.concat([final_run_list, run_list])

if args.verbose:
    print("Final run list to be analysed")
    print(final_run_list)

# manage the target
ra, dec, name = get_coordinates(args)

# manage the cuts
if args.cut is not None:
    args.cut = config_file['cut_folder']+"/"+args.cut
    print("Using cut file:", args.cut)
else:
    print("Standard cuts will be used in the analysis")

# output directory
out_dir = config_file['output_folder']
if args.outdir is not None:
    out_dir = args.outdir

# check if directory exists, if not, create it
if not (os.path.isdir(out_dir)):
    try:
        os.makedirs(out_dir, exist_ok=True)
        print(f'Output directory {out_dir} created successfully')
    except OSError:
        print(f'Output directory {out_dir} can not be created')
else:
    print(f'Data will be stored in {out_dir}')

# cycle on each file
#savedRunlist = open(out_dir+"/Myrunlist_"+str(datetime.datetime.today().year) +
#                    str(datetime.datetime.today().month) +
#                    str(datetime.datetime.today().day) + ".txt", "w")
for run_dl3 in final_run_list.index:
    # write the run list
    savedRunlist.write(str(run_dl3)+"\n")
    night = database_file.loc[run_dl3, 'day']
    dl2_file = final_run_list.loc[run_dl3, 'path']
    dl3_output_path = join(out_dir, str(night))

    dl3_cmd = f'lstchain_create_dl3_file \
    -d {dl2_file} \
    -o {dl3_output_path} \
    --input-irf {config_file['irf_file']} \
    --source-name {name} \
    --source-ra "{ra}deg" \
    --source-dec "{dec}deg"' \
    + ['', ' --overwrite '][args.globber is not None] \
    + ['', f' --config {args.cut_file}'][args.cut_file is not None]

if args.gh_cut:
    dl3_cmd += f" --fixed-gh-cut {args.gh_cut}"
if args.theta_cut:
    dl3_cmd += f" --fixed-theta-cut {args.theta_cut}"

# cut files not well managed
    if args.verbose:
        print('\n')
        print(dl3_cmd)
    if not(args.dry):
        if args.submit:
            # create the script that will be submited and return the name of the script in the variable scriptname
            scriptname = manage_submission(args, config_file, dl3_cmd, run_dl3)
            os.system("sbatch "+scriptname)
        else:
            print('Interactive conversion of the DL2 to DL3\n')
            os.system(dl3_cmd)

#savedRunlist.close()
