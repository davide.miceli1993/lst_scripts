import os
import json
from os.path import join

from utils import (parse_arguments,
                   get_config)

# check that the environment variables are set
#if os.environ.get('CODE_DIR') is None:
#    raise RuntimeError("Set the variable CODE_DIR")

# argument parser
args = parse_arguments(description="Create IRF from MC files",
                       add_irf=True,
                       add_job=True,
                       )

config_file = get_config(args.config)

# check that command-line values don't override config file ones
if (args.theta_cut or args.gh_cut or args.obs_time) and (args.config is not None):
    print('If IRF config file is provided, no theta, gh or time cut can be given from command-line.\
            Provide only one of the two to avoid overwrinting values.')
    exit()

# variables definition
off = args.offset
if off not in ('0.0', '0.4'):
    raise ValueError('Offest value is not valid')

# in case of custom config file
if args.config is not None:
    f = open(args.config)
    irf_config = json.load(f)
    gh_cut = irf_config['DL3FixedCuts']['fixed_gh_cut']
elif args.gh_cut is not None:
    gh_cut = args.gh_cut
else:
    gh_cut = 'Std'

Zd = config_file['zenith']
version = config_file['version']
direction = config_file['direction']
gamma_type = config_file['gammatype']

# fix for typo in MC path
if Zd == 40 and version == '20210416_v0.7.3_prod5_trans_80_local_taicut_8_4':
    version = '20210506_v0.7.3_prod5_trans_80_zen40deg_local_tailcut_8_4'

# check existance of directories
dl2_dir = config_file['DL2_dir']
if not(os.path.exists(dl2_dir)):
    raise RuntimeError("DL2 directory does not exist")

# get Az from direction
if direction == 'south':
    Az = 180
elif direction == 'north':
    Az = 0
else:
    raise ValueError('Variable direction is not valid')

# select MC files
if gamma_type == 'point':
    gamma_mc = f'{dl2_dir}/gamma/zenith_{Zd}deg/{direction}_pointing/{version}/off{off}deg/dl2_gamma_{Zd}deg_{Az}deg_off{off}deg_{version}_testing.h5'
elif gamma_type == 'diffuse':
    gamma_mc = f'{dl2_dir}/gamma-diffuse/zenith_${Zd}deg/${direction}_pointing/{version}/dl2_gamma-diffuse_{Zd}deg_{Az}deg_{version}_testing.h5'
else:
    raise ValueError("Invalid gamma type")
if os.path.isfile(gamma_mc):
    if args.verbose:
        print('##################')
        print('MC gamma file selected:')
        print(gamma_mc)
        print('##################')
else:
    print("MC gamma file does not exist")

proton_mc = f'{dl2_dir}/proton/zenith_{Zd}deg/{direction}_pointing/{version}/dl2_proton_{Zd}deg_{Az}deg_{version}_testing.h5'
electron_mc = f'{dl2_dir}/electron/zenith_{Zd}deg/{direction}_pointing/{version}/dl2_electron_{Zd}deg_{Az}deg_{version}_testing.h5'
if os.path.isfile(proton_mc):
    if args.verbose:
        print('MC proton file selected:')
        print(proton_mc)
        print('##################')
else:
    print("MC proton file does not exist")
if os.path.isfile(electron_mc):
    if args.verbose:
        print('MC electron file selected:')
        print(electron_mc)
        print('##################')
else:
    print("MC electron file does not exist")

# define IRF file name
irf_filename = f'irf_{version}_Zd{Zd}deg_{direction}_off{off}deg_ghcut{gh_cut}.fits.gz'
if args.verbose:
    print('IRF file:')
    print(irf_filename)
    print('##################')

# output directory
out_dir = config_file['output_folder']
if args.outdir is not None:
    out_dir = args.outdir

# check if directory exists, if not, create it
if not (os.path.isdir(out_dir)):
    try:
        os.makedirs(out_dir, exist_ok=True)
        print(f'Output directory {out_dir} created successfully')
    except OSError:
        print(f'Output directory {out_dir} can not be created')
else:
    print(f'Data will be stored in {out_dir}')

irf_file = join(out_dir, irf_filename)
irf_cmd = f'lstchain_create_irf_files \
            --input-proton-dl2 {proton_mc} \
            --input-electron-dl2 {electron_mc} \
            --input-gamma-dl2 {gamma_mc} \
            --output-irf-file {irf_file}' \
            + ['', f' --config {args.config}'][args.config is not None] \
            + ['', ' --overwrite '][args.globber is not None] \
            + ['', ' --point-like '][gamma_type == 'point']
if args.gh_cut:
    irf_cmd += f" --fixed-gh-cut {args.gh_cut}"
if args.theta_cut:
    irf_cmd += f" --fixed-theta-cut {args.theta_cut}"
if args.obs_time:
    irf_cmd += f" --irf-obs-time {args.obs_time}"

if args.verbose:
    print('##################')
    print('Generation of IRF')
    print(irf_cmd)
if not (args.dry):
    os.system(irf_cmd)
